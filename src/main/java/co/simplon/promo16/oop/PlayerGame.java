package co.simplon.promo16.oop;

import java.util.Scanner;

public class PlayerGame {
    public static boolean isOver;
    public Grid newGame;
    boolean over = false;
    Grid grid;

    public PlayerGame(Grid newGame) {
        this.newGame = newGame;
    }

    public PlayerGame() {
        this.newGame = new Grid(6, 6);
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);

        while (!over) {
            System.out.println("Entrer le numéro de la ligne souhaitée (entre 1 et 6) :");
            int r = scanner.nextInt();
            System.out.println("Entrer le numéro de la colonne souhaitée (entre 1 et 6) :");
            int c = scanner.nextInt();
            System.out.println("Entrer 1 ou 0 en respectant les consignes du jeu :");
            char t = scanner.next().charAt(0);
            newGame.place(r, c, t);
            newGame.display();
            over = newGame.victory();
        }
        System.out.println("BRAVO !! C'est gagné !"+ "\n");
        scanner.close();
    }
}
